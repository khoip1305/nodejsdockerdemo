const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send("This is a really new feature of version 0.0.1!");
});

app.listen(4000, () => {
  console.log("Listening on port:\nhttp://localhost:4000");
});
